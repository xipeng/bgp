This repo contains the essential code for the paper "Using Canonical Codes to Efficiently Solve the Benzenoid Generation Problem with Constraint Programming". 



**Files description:**

In the article, we discussed two application cases: 

1. enumerate all hexagon graphs without single vertex holes, catacondensed  or not (BGP1  and BGP2)
2. enumerate the hexagons graphs that contains one or two given patterns (BGP3 and BGP4)

In the folder "src/" : "ProCanonicity.java" and "ProCanonicity1.java" are used to solve case 1, "ProPrefixCanonicity.java" and "ProPrefixCanonicity1.java" are used to solve case 2. Our global constraint is based on a posteriori verification, and is not idempotent, so we've implemented a redundant propagator in each case to make it idempotent. 



**Dependencies:**

In this work, the Choco version is 4.10.9.