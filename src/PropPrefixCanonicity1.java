import org.chocosolver.memory.IEnvironment;
import org.chocosolver.memory.IStateInt;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.events.PropagatorEventType;
import org.chocosolver.util.ESat;
import org.chocosolver.util.tools.ArrayUtils;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Objects;

public class PropPrefixCanonicity1 extends Propagator<IntVar> {

    private final int n;
    private final String pattern1, pattern2;

    private final int pattern_degree1, pattern_degree2;

    private final int[] pattern_code1, pattern_code2, forbidden_code1, forbidden_code2;

    private final int pattern_length1, forbidden_code_length1;

    private final IStateInt alpha;
    private final IntVar[] pred;
    private final IntVar[] dir;
    private final IntVar[] x;
    private final IntVar[] y;
    private IStateInt[][] graph;
    private IStateInt[] current_code;

    private IStateInt[] degree;
    private IStateInt[] pattern;

    private boolean is_catacondensed;

    public PropPrefixCanonicity1(IntVar[] Pred, IntVar[] Dir, IntVar[] X, IntVar[] Y, String p1, String p2, boolean is_catacondensed){
        super(ArrayUtils.append(Pred, Dir, X, Y), PropagatorPriority.LINEAR, true);
        this.n = X.length;

        this.pattern1 = p1;
        this.pattern2 = p2;
        this.pattern_degree1 = readPatternDegree(p1);
        this.pattern_degree2 = readPatternDegree(p2);
        this.pattern_code1 = readPatternCode(p1);
        this.pattern_code2 = readPatternCode(p2);
        this.forbidden_code1 = readPatternForbiddenCode(p1);
        this.forbidden_code2 = readPatternForbiddenCode(p2);
        this.pattern_length1 = this.pattern_code1.length/2+1;
        this.forbidden_code_length1 = this.forbidden_code1.length/2;


        this.pred = Arrays.copyOfRange(vars, 0, n);
        this.dir = Arrays.copyOfRange(vars, n, 2*n);
        this.x = Arrays.copyOfRange(vars, 2*n, 3*n);
        this.y = Arrays.copyOfRange(vars, 3*n, 4*n);
        IEnvironment environment = model.getEnvironment();
        this.alpha = environment.makeInt(this.pattern_length1-1);
        this.graph = new IStateInt[n][6];
        this.current_code = new IStateInt[2*(n-this.pattern_length1)];
        this.degree = new IStateInt[n];
        this.pattern = new IStateInt[n];
        this.is_catacondensed = is_catacondensed;


        for(int i=0; i< n; i++){
            for(int j =0; j<6; j++){
                this.graph[i][j] = environment.makeInt(-1);
            }
            this.pattern[i] = environment.makeInt(-1);
        }

        for(int i=0; i<this.current_code.length;i++) {
            this.current_code[i] = environment.makeInt(-1);
        }

        for(int i=0; i<n; i++){
            this.degree[i] = environment.makeInt(0);
        }
    }

    @Override
    public void propagate(int evtmask) throws ContradictionException {
        boolean full_instantiated = true;
        for(int i=0; i<4*n; i++){
            if(!vars[i].isInstantiated()){
                full_instantiated = false;
                break;
            }
        }
        if(full_instantiated){
            for(int i=0; i<this.pattern_length1; i++){
                if(!updateGraph(i)){
                    fails();
                }
            }
            int indice= this.pattern_length1;
            while (indice < n) {
                // incrementally construct the graph: first find the auxiliaire edges
                current_code[2*(indice-this.pattern_length1)].set(pred[indice].getValue());
                current_code[2*(indice-this.pattern_length1)+1].set(dir[indice].getValue());
                if(!updateGraph(indice)){
                    fails();
                }
                if(!checkCanonic(indice)){
                    fails();
                } else {
                    // update alpha
                    alpha.set(indice);
                    indice++;
                }
            }
            if(!Objects.equals(this.pattern2, " ")) {
                if (!containSecPattern()) {
                    fails();
                }
            }
        }
    }

    @Override
    public void propagate(int varIdx, int mask) throws ContradictionException{
        forcePropagate(PropagatorEventType.FULL_PROPAGATION);
    }

    @Override
    public ESat isEntailed() {
        if (isCompletelyInstantiated()) {
            for(int i=this.alpha.get(); i<n-1; i++){
                if(!updateGraph(i+1)){
                    return ESat.FALSE;
                }
                this.alpha.set(i+1);
            }
            for(int i=this.pattern_length1; i<n;i++){
                current_code[2*(i-this.pattern_length1)].set(pred[i].getValue());
                current_code[2*(i-this.pattern_length1)+1].set(dir[i].getValue());
            }

            for(int i=this.pattern_length1; i<n; i++){
                if(!checkCanonic(i)){
                    return ESat.FALSE;
                }
            }
            if(!Objects.equals(this.pattern2, " ")) {
                if (!containSecPattern()) {
                    return ESat.FALSE;
                }
            }
            return ESat.TRUE;
        }
        return ESat.UNDEFINED;
    }

    private boolean updateGraph(int i) {
        for (int j = 0; j < i; j++) {
            // the hexagone case
            if (x[i].getValue() - x[j].getValue() == 2 && y[i].getValue() == y[j].getValue()) {
                this.graph[j][0].set(i);
                this.graph[i][3].set(j);
            } else if (x[i].getValue() - x[j].getValue() == 1 && y[i].getValue() - y[j].getValue() == 1) {
                this.graph[j][1].set(i);
                this.graph[i][4].set(j);
            } else if (x[i].getValue() - x[j].getValue() == -1 && y[i].getValue() - y[j].getValue() == 1) {
                this.graph[j][2].set(i);
                this.graph[i][5].set(j);
            } else if (x[i].getValue() - x[j].getValue() == -2 && y[i].getValue() == y[j].getValue()) {
                this.graph[j][3].set(i);
                this.graph[i][0].set(j);
            } else if (x[i].getValue() - x[j].getValue() == -1 && y[i].getValue() - y[j].getValue() == -1) {
                this.graph[j][4].set(i);
                this.graph[i][1].set(j);
            } else if (x[i].getValue() - x[j].getValue() == 1 && y[i].getValue() - y[j].getValue() == -1) {
                this.graph[j][5].set(i);
                this.graph[i][2].set(j);
            } else {
                continue;
            }
            int degree_i = this.degree[i].get();
            this.degree[i].set(degree_i+1);
            int degree_j = this.degree[j].get();
            this.degree[j].set(degree_j+1);
            updatePattern(j);
            updatePattern(i);
            if(is_catacondensed){
                if(this.pattern[i].get()<9 ){
                    return false;
                }
                if(this.pattern[j].get()<9){
                    return false;
                }
            }
        }
        // check holes
        return !containsHole(i);

    }

    public boolean containSecPattern(){
        // step1: create a new adjacent map from the current graph: from the position of the motif. In this new map, set the vertices of pattern 1 and its forbidden
        //        vertices as -2
        // step2: chose a vertice which has at least the same num of neighbors as the vertex 0 in the pattern2, starting from one of its neihbors
        //        to check if the same graph subgraph exsit

        for(int i=0; i < n; i++){
            // check if there is an occurence of the pattern with beginning from k
            if(this.degree[i].get() >= this.pattern_degree1){
                for(int j=0; j<6; j++){
                    if(this.graph[i][j].get()>=0){

                        if(containPattern2(i, j, 0, this.pattern_degree2, this.pattern_code2, this.forbidden_code2)) {
                            return true;
                        }
                        if(containPattern2(i, j, 1, this.pattern_degree2, this.pattern_code2, this.forbidden_code2)) {
                            return true;
                        }

                    }
                }
            }
        }
        return false;
    }

    private boolean containPattern2(int i, int direction, int orientation, int degree, int[] code_base, int[] forbidden_code){
        // [i, direction, orientation] a check from the i^th hexagon, taking the direction as 0 form the given orientation
        // degree: the degree of nb0 hexagon in pattern2; code_base: the code of pattern 2; forbbiden_code: the forbidden_code of pattern2
        int[] visit_order = new int[this.pattern_length1];
        int[] forbidden_x = new int[this.forbidden_code_length1];
        int[] forbidden_y = new int[this.forbidden_code_length1];
        visit_order[0] = i;
        // traverse graph
        for(int k=0; k<this.pattern_length1-1;k++){
            int index;
            if(orientation==0){
                index = (this.pattern_code1[2*k+1]+direction)%6;
            } else {
                index = (direction-this.pattern_code1[2*k+1]+6)%6;
            }
            if(this.graph[visit_order[this.pattern_code1[2*k]]][index].get() < 0){
                return false;
            } else {
                visit_order[k+1] = this.graph[visit_order[this.pattern_code1[2*k]]][index].get();
            }
        }
        // check forbidden edge
        for(int k=0; k< this.forbidden_code_length1; k++){
            int index;
            if(orientation==0){
                index = (this.forbidden_code1[2*k+1]+direction)%6;
            } else {
                index = (direction-this.forbidden_code1[2*k+1]+6)%6;
            }
            if(index==0) {
                forbidden_x[k] = this.x[visit_order[this.forbidden_code1[2*k]]].getValue()+2;
                forbidden_y[k] = this.y[visit_order[this.forbidden_code1[2*k]]].getValue();
            } else if(index==1) {
                forbidden_x[k] = this.x[visit_order[this.forbidden_code1[2*k]]].getValue()+1;
                forbidden_y[k] = this.y[visit_order[this.forbidden_code1[2*k]]].getValue()+1;
            } else if(index==2) {
                forbidden_x[k] = this.x[visit_order[this.forbidden_code1[2*k]]].getValue()-1;
                forbidden_y[k] = this.y[visit_order[this.forbidden_code1[2*k]]].getValue()+1;
            } else if(index==3) {
                forbidden_x[k] = this.x[visit_order[this.forbidden_code1[2 * k]]].getValue() - 2;
                forbidden_y[k] = this.y[visit_order[this.forbidden_code1[2 * k]]].getValue();
            } else if(index==4) {
                forbidden_x[k] = this.x[visit_order[this.forbidden_code1[2 * k]]].getValue() - 1;
                forbidden_y[k] = this.y[visit_order[this.forbidden_code1[2 * k]]].getValue() -1;
            } else {
                forbidden_x[k] = this.x[visit_order[this.forbidden_code1[2 * k]]].getValue() +1;
                forbidden_y[k] = this.y[visit_order[this.forbidden_code1[2 * k]]].getValue() -1;
            }

            if(this.graph[visit_order[this.forbidden_code1[2*k]]][index].get() != -1){
                return false;
            }
        }

        // if pattern1 exsits, create a map
        int[] xprime = new int[n+this.forbidden_code_length1];
        int[] yprime = new int[n+this.forbidden_code_length1];
        for(int j=0; j<n; j++) {
            xprime[j] = x[j].getValue();
            yprime[j] = y[j].getValue();
        }
        for(int j=0;j<this.forbidden_code_length1;j++) {
            xprime[j+n] = forbidden_x[j];
            yprime[j+n] = forbidden_y[j];
        }
        int[][] graph_prime = new int[n+this.forbidden_code_length1][6];
        for(int k=0; k< graph_prime.length; k++){
            for(int j =0; j<6; j++){
                graph_prime[k][j] = -1;
            }
        }

        for(int k=0; k<n+this.forbidden_code_length1; k++){
            updateAdjacency(k, xprime, yprime, graph_prime);
        }

        // step 2 remove the vertices associated with the pattern 1 as well as its forbidden voisins
        for(int k=0; k<this.pattern_length1;k++) {
            for(int j=0; j<6; j++) {
                if (graph_prime[visit_order[k]][j] >= 0) {
                    graph_prime[graph_prime[visit_order[k]][j]][(j + 3) % 6] = -2;
                    graph_prime[visit_order[k]][j] = -2;
                }
            }
        }
        for(int k=n; k<graph_prime.length; k++){
            for(int j =0; j<6; j++){
                if(graph_prime[k][j]>=0){
                    graph_prime[graph_prime[k][j]][(j+3)%6] = -2;
                    graph_prime[k][j] = -2;
                }
            }
        }

        int[] nb_succ = new int[xprime.length];
        for(int k=0; k<xprime.length; k++){
            int nb = 0;
            for(int j =0; j<6; j++){
                if(graph_prime[k][j]>=0){
                    nb++;
                }
            }
            nb_succ[k] = nb;
        }
        for(int k=0; k<n; k++){
            if(nb_succ[k] >= degree){
                for(int j=0; j<6; j++){
                    if(graph_prime[k][j]>=0){
                        if(existSubgraph(k, j, 0, graph_prime, code_base, forbidden_code)){
                            return true;
                        }
                        if(existSubgraph(k, j, 1, graph_prime, code_base, forbidden_code)){
                            return true;
                        }

                    }
                }
            }
        }
        return false;
    }

    private static void updateAdjacency(int i, int[] x, int[] y, int[][] graph) {
        for (int j = 0; j < i; j++) {
            if (x[i] - x[j] == 2 && y[i] == y[j]) {
                graph[j][0]= i;
                graph[i][3]= j;
            } else if (x[i] - x[j] == 1 && y[i] - y[j] == 1) {
                graph[j][1]= i;
                graph[i][4]= j;
            } else if (x[i] - x[j] == -1 && y[i] - y[j] == 1) {
                graph[j][2]= i;
                graph[i][5]= j;
            } else if (x[i] - x[j] == -2 && y[i] == y[j]) {
                graph[j][3]= i;
                graph[i][0]= j;
            } else if (x[i] - x[j] == -1 && y[i] - y[j] == -1) {
                graph[j][4]= i;
                graph[i][1]= j;
            } else if (x[i] - x[j] == 1 && y[i] - y[j] == -1) {
                graph[j][5]= i;
                graph[i][2]= j;
            }
        }
    }

    private static boolean existSubgraph(int i, int direction, int orientation, int[][] graph, int[] code_base, int[] forbidden_code){
        // [i, direction, orientation] a check from the i^th hexagon, taking the direction as 0 form the given orientation

        int code_length = code_base.length/2;
        int[] visit_order = new int[code_length+1];
        visit_order[0] = i;
        // traverse graph
        for(int k=0; k<code_length;k++){
            int index;
            if(orientation==0){
                index = (code_base[2*k+1]+direction)%6;
            } else {
                index = (direction-code_base[2*k+1]+6)%6;
            }
            if(graph[visit_order[code_base[2*k]]][index] < 0){
                return false;
            } else {
                visit_order[k+1] = graph[visit_order[code_base[2*k]]][index];
            }
        }
        // check forbidden edge
        int forbidden_code_length = forbidden_code.length/2;
        for(int k=0; k< forbidden_code_length; k++){
            int index;
            if(orientation==0){
                index = (forbidden_code[2*k+1]+direction)%6;
            } else {
                index = (direction-forbidden_code[2*k+1]+6)%6;
            }
            if(graph[visit_order[forbidden_code[2*k]]][index] != -1){
                return false;
            }
        }
        return true;
    }
    public boolean containsHole(int k){
        if(k<5){
            return false;
        }
        // we consider the vertex which has pattern in {1, 3, 5, 6,7, 9, 10}
        if(this.pattern[k].get()>10){
            return false;
        } else if(this.pattern[k].get()==1 || this.pattern[k].get()==3 || this.pattern[k].get()==5 || this.pattern[k].get()==9) {
            for(int d=0; d<6; d++){
                if(this.graph[k][d].get()==-1){
                    int d1 = (d+5)%6;
                    int v1 = this.graph[k][d1].get();
                    int d2 = (d+1)%6;
                    int v2 = this.graph[k][d2].get();
                    int v3 = this.graph[v1][d].get();
                    int v4 = this.graph[v2][d].get();

                    if(v3==-1 || v4==-1){
                        continue;
                    } else {
                        int v5 = this.graph[v3][d2].get();
                        int v6 = this.graph[v4][d1].get();
                        if(v5!=-1 && v6!=-1){
                            return true;
                        }
                    }
                }
            }
        } else if(this.pattern[k].get()==6 || this.pattern[k].get()==7 || this.pattern[k].get()==10) {
            for(int d=0; d<6; d++){
                if(this.graph[k][d].get()==-1 && this.graph[k][(d+5)%6].get()>=0 && this.graph[k][(d+1)%6].get()>=0){
                    int d1 = (d+5)%6;
                    int v1 = this.graph[k][d1].get();
                    int d2 = (d+1)%6;
                    int v2 = this.graph[k][d2].get();
                    int v3 = this.graph[v1][d].get();
                    int v4 = this.graph[v2][d].get();

                    if(v3==-1 || v4==-1){
                        break;
                    } else {
                        int v5 = this.graph[v3][d2].get();
                        int v6 = this.graph[v4][d1].get();
                        if(v5!=-1 && v6!=-1){
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }


    private void updatePattern(int i){
        // 0: d_6 < 1: d_5 < 2: d_4(4) < 3: d_4(3+1) < 4: d_3(3) < 5: d_4(2+2) < 6: d_3(2+1a) = 7: d_3(2+1b) < 8: d_2(2) < 9: d_3(1+1+1) < 10: d_2(1+1a) < 11: d_2(1+1b) < 12: d_1
        // we treat 6,7 as the same case
        switch(this.pattern[i].get()){
            case 12:
                for(int j=0; j<6; j++){
                    if(this.graph[i][j].get()>=0 && this.graph[i][(j+1)%6].get()>=0){
                        this.pattern[i].set(8);
                        return;
                    }
                    if(this.graph[i][j].get()>=0 && this.graph[i][(j+3)%6].get()>=0){
                        this.pattern[i].set(11);
                        return;
                    }
                }

                this.pattern[i].set(10);
                break;
            case 11:
                this.pattern[i].set(6);
                break;

            case 10:
                for(int j=0; j<6; j++){
                    if(this.graph[i][j].get()>=0 && this.graph[i][(j+1)%6].get()>=0 && this.graph[i][(j+2)%6].get()>=0){
                        this.pattern[i].set(4);
                        return;
                    } else if (this.graph[i][j].get()>=0 && this.graph[i][(j+2)%6].get()>=0 && this.graph[i][(j+4)%6].get()>=0){
                        this.pattern[i].set(9);
                        return;
                    }
                }
                this.pattern[i].set(6);
                break;
            case 9:
                this.pattern[i].set(3);
                break;
            case 8:
                for(int j=0; j<6; j++){
                    if(this.graph[i][j].get()>=0 && this.graph[i][(j+1)%6].get()>=0 && this.graph[i][(j+2)%6].get()>=0){
                        this.pattern[i].set(4);
                        return;
                    }
                }

                this.pattern[i].set(6);
                break;
            case 6: // case 6 and case 7 are the same
                for(int j=0; j<6; j++){
                    if(this.graph[i][j].get()<0 && this.graph[i][(j+1)%6].get()<0){
                        this.pattern[i].set(2);
                        return;
                    } else if(this.graph[i][j].get()<0 && this.graph[i][(j+2)%6].get()<0){
                        this.pattern[i].set(3);
                        return;
                    }
                }

                this.pattern[i].set(5);
                break;
            case 5:
            case 2:
            case 3:
                this.pattern[i].set(1);
                break;
            case 4:
                for(int j=0; j<6; j++){
                    if(this.graph[i][j].get()<0 && this.graph[i][(j+1)%6].get()<0){
                        this.pattern[i].set(2);
                        return;
                    }
                }
                this.pattern[i].set(3);
                break;
            case 1:
                this.pattern[i].set(0);
                break;
            case -1:
                this.pattern[i].set(12);
                break;
            default:
                throw new AssertionError("pattern error");
        }
    }

    private boolean checkCanonic(int i){
        if(i<this.n-1){
            // remove dominated code
            return checkCanonic2(i);
        } else{
            // check all the occurrences to ensure the p-canonicity
            return checkCanonic1(i);
        }
    }

    private boolean checkCanonic1(int i){

        if(i<this.pattern_length1){
            return true;
        } else {
            for(int k=0; k<=i; k++){
                // check if there is an occurence of the pattern with beginning from k
                if(this.degree[k].get() >= this.pattern_degree1){
                    for(int j=0; j<6; j++){
                        if(this.graph[k][j].get()>=0){
                            int check_result1 = checkSubmorphic(i, k, j, 0);
                            if(check_result1 == 2){
                                return false;
                            }
                            int check_result2 = checkSubmorphic(i, k, j, 1);
                            if(check_result2 == 2){
                                return false;
                            }

                        }
                    }
                }
            }
        }
        return true;
    }

    private boolean checkCanonic2(int i){
        if(i<this.pattern_length1){
            return true;
        } else {
            int[] code1, code2;
            if(Objects.equals(this.pattern1, "deepbay")) {
                int [] permu1 = {0, 1, 2, 3};
                int [] permu2 = {1, 0, 3, 2};
                code1 = prefixBfs(permu1, 0, 0);
                code2 = prefixBfs(permu2, 3, 1);
                return (compareSign(current_code, code1, i - this.pattern_length1 + 1)) && (compareSign(current_code, code2, i - this.pattern_length1 + 1));
            } else if(Objects.equals(this.pattern1, "a")){
                int[] permu1 = {0, 1, 2};
                int[] permu2 = {0, 2, 1};
                code1 = prefixBfs(permu1, 0, 0);
                code2 = prefixBfs(permu2, 2, 1);
                return (compareSign(current_code, code1, i - this.pattern_length1 + 1)) && (compareSign(current_code, code2, i - this.pattern_length1 + 1));
            } else if(Objects.equals(this.pattern1, "b")){
                int[] permu1 = {0, 1, 2};
                int[] permu2 = {0, 2, 1};


                code1 = prefixBfs(permu1, 0, 0);
                code2 = prefixBfs(permu2, 1, 1);


                return (compareSign(current_code, code1, i - this.pattern_length1 + 1)) && (compareSign(current_code, code2, i - this.pattern_length1 + 1));

            } else if(Objects.equals(this.pattern1, "c")){
                int[] permu1 = {0, 1};
                code1 = prefixBfs(permu1, 0, 0);

                return compareSign(current_code, code1, i - this.pattern_length1 + 1);
            }else if(Objects.equals(this.pattern1, "d")) {
                int[] permu1 = {0, 1, 2};
                int[] permu2 = {0, 2, 1};

                code1 = prefixBfs(permu1, 0, 0);
                code2 = prefixBfs(permu2, 2, 1);

                return (compareSign(current_code, code1, i - this.pattern_length1 + 1)) && (compareSign(current_code, code2, i - this.pattern_length1 + 1));
            } else if(Objects.equals(this.pattern1, "e")) {
                int[] permu1 = {0, 1, 2, 3, 4};
                int[] permu2 = {0, 2, 1, 4, 3};

                code1 = prefixBfs(permu1, 0, 0);
                code2 = prefixBfs(permu2, 2, 1);

                return (compareSign(current_code, code1, i - this.pattern_length1 + 1)) && (compareSign(current_code, code2, i - this.pattern_length1 + 1));
            } else if(Objects.equals(this.pattern1, "f")) {
                int[] permu1 = {0, 1};
                int[] permu2 = {1, 0};

                code1 = prefixBfs(permu1, 0, 0);
                code2 = prefixBfs(permu2, 3, 1);

                return (compareSign(current_code, code1, i - this.pattern_length1 + 1)) && (compareSign(current_code, code2, i - this.pattern_length1 + 1));
            } else if(Objects.equals(this.pattern1, "g")) {
                int[] permu1 = {0, 1, 2};
                int[] permu2 = {0, 2, 1};

                code1 = prefixBfs(permu1, 0, 0);
                code2 = prefixBfs(permu2, 3, 1);

                return (compareSign(current_code, code1, i - this.pattern_length1 + 1)) && (compareSign(current_code, code2, i - this.pattern_length1 + 1));
            } else if(Objects.equals(this.pattern1, "nsy")){
                int[] permu1 = {0, 1, 2, 3};
                code1 = prefixBfs(permu1, 0, 0);
                return compareSign(current_code, code1, i - this.pattern_length1 + 1);
            }
        }
        return true;
    }

    private int checkSubmorphic(int indice, int i, int direction, int orientation){
        // [i, direction, orientation] a check from the i^th hexagon, taking the direction as 0 form the given orientation
        // return 1: non submorphic, return 2: submorphic && non-canonic  return 3: submorphic canonic
        int code_length = this.pattern_length1-1;
        int[] visit_order = new int[this.pattern_length1];
        visit_order[0] = i;
        // traverse graph
        for(int k=0; k<code_length;k++){
            int index;
            if(orientation==0){
                index = (this.pattern_code1[2*k+1]+direction)%6;
            } else {
                index = (direction-this.pattern_code1[2*k+1]+6)%6;
            }
            if(this.graph[visit_order[this.pattern_code1[2*k]]][index].get() < 0){
                return 1;
            } else {
                visit_order[k+1] = this.graph[visit_order[this.pattern_code1[2*k]]][index].get();
            }
        }
        // check forbidden edge
        for(int k=0; k< this.forbidden_code_length1; k++){
            int index;
            if(orientation==0){
                index = (this.forbidden_code1[2*k+1]+direction)%6;
            } else {
                index = (direction-this.forbidden_code1[2*k+1]+6)%6;
            }
            if(this.graph[visit_order[this.forbidden_code1[2*k]]][index].get() != -1){
                return 1;

            }
        }

        int[] code1 = prefixBfs(visit_order, direction, orientation);
        if ((!compareSign(current_code, code1, indice - this.pattern_length1 + 1))) {
            return 2;
        }
        return 3;
    }


    private boolean compareSign(IStateInt[] c1, int[] c2, int num){
        /*the size of code is num*2,  return true if c1 <= c2 */
        int k= 0;
        while(k < num){
            if(c1[2*k].get() > c2[2*k]){
                return false;
            } else if (c1[2*k].get() < c2[2*k]) {
                return true;
            } else {
                if(c1[2*k+1].get() > c2[2*k+1]){
                    return false;
                } else if (c1[2*k+1].get() < c2[2*k+1]){
                    return true;
                }
            }
            k++;
        }
        return true;
    }
    private int[] prefixBfs(int[] permu, int direction, int orientation){
        // s: source, direction : [0, 1, 2, 3, 4, 5], orientation: ccw=0, cw=1
        LinkedList<Integer> q = new LinkedList<>();
        boolean[] visited = new boolean[n];
        int[] visited_order = new int[n];
        int[] code = new int[2*(n-this.pattern_length1)];
        int code_cnt = 0;
        int order = 0;

        for(int i=0; i<this.pattern_length1; i++){
            q.add(permu[i]);
            visited_order[permu[i]] = order;
            order++;
            visited[permu[i]] = true;
        }

        while (!q.isEmpty()) {
            int v = q.poll();
            for(int i=0; i<6;i++) {
                int index;
                if(orientation==0){
                    index = (i+direction)%6;
                } else {
                    index = (direction-i+6)%6;
                }
                int u = this.graph[v][index].get();
                if (u >= 0 && !visited[u]) {
                    visited[u] = true;
                    q.add(u);
                    visited_order[u]=order;
                    order++;

                    code[code_cnt++] = visited_order[v];
                    code[code_cnt++]=i;
                }
            }
        }
        return code;
    }


    public static int readPatternDegree(String p) {
        int degree;
        if (Objects.equals(p, "deepbay")) {
            degree = 2;
        } else if (Objects.equals(p, "a")) {
            degree = 2;
        } else if (Objects.equals(p, "b")) {
            degree = 2;
        } else if (Objects.equals(p, "c")) {
            degree = 1;
        } else if (Objects.equals(p, "d")) {
            degree = 2;
        } else if (Objects.equals(p, "e")) {
            degree = 2;
        } else if (Objects.equals(p, "f")) {
            degree = 1;
        } else if (Objects.equals(p, "g")) {
            degree = 2;
        } else if (Objects.equals(p, "nsy")) {
            degree = 3;
        } else if (Objects.equals(p, " ")) {
            degree = 0;
        } else {
            System.out.println("pattern error");
            return -1;
        }
        return degree;
    }

    public static int[] readPatternCode(String p) {
        int[] pattern_code;
        if (Objects.equals(p, "deepbay")) {
            pattern_code = new int[]{0, 0, 0, 2, 1, 1};
        } else if (Objects.equals(p, "a")) {
            pattern_code = new int[]{0, 0, 0, 2};
        } else if (Objects.equals(p, "b")) {
            pattern_code = new int[]{0, 0, 0, 1};
        } else if (Objects.equals(p, "c")) {
            pattern_code = new int[]{0, 0};
        } else if (Objects.equals(p, "d")) {
            pattern_code = new int[]{0, 0, 0, 2};
        } else if (Objects.equals(p, "e")) {
            pattern_code = new int[]{0, 0, 0, 2, 1, 1, 2, 1};
        } else if (Objects.equals(p, "f")) {
            pattern_code = new int[]{0, 0};
        } else if (Objects.equals(p, "g")) {
            pattern_code = new int[]{0, 0, 0, 3};
        } else if(Objects.equals(p, "nsy")){
            pattern_code = new int[]{0, 0, 0, 1, 0, 3};
        }
        else if (Objects.equals(p, " ")) {
            pattern_code = new int[]{};
        } else {
            System.out.println("pattern error");
            return new int[]{};
        }
        return pattern_code;
    }

    public static int[] readPatternForbiddenCode(String p){
        int[] forbidden_code;
        if(Objects.equals(p, "deepbay")){
            forbidden_code = new int[] {0, 1, 2, 1,3, 2};
        } else if(Objects.equals(p, "a")){
            forbidden_code = new int[] {0, 3, 0, 4, 0, 5};
        } else if(Objects.equals(p, "b")){
            forbidden_code = new int[] {0, 2, 0, 3, 0, 4, 0, 5};
        }
        else if(Objects.equals(p, "c")){
            forbidden_code = new int[] {0, 1, 0, 5, 1, 0, 1, 1, 1, 5};
        } else if(Objects.equals(p, "d")){
            forbidden_code = new int[] {0, 1, 1, 1, 2, 1};
        } else if(Objects.equals(p, "e")) {
            forbidden_code = new int[] {0, 1, 3, 1, 4, 1};
        } else if(Objects.equals(p, "f")) {
            forbidden_code = new int[] {0, 1, 0, 2, 1, 1};
        } else if(Objects.equals(p, "g")) {
            forbidden_code = new int[] {0, 1, 0, 2};
        } else if(Objects.equals(p, "nsy")){
            forbidden_code = new int[] {0, 4, 0, 5};
        }
        else if(Objects.equals(p, " ")) {
            forbidden_code = new int[] {};
        } else{
            System.out.println("pattern error");
            return new int[] {};
        }
        return forbidden_code;
    }

}
