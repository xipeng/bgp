import org.chocosolver.memory.IEnvironment;
import org.chocosolver.memory.IStateInt;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.events.PropagatorEventType;
import org.chocosolver.util.ESat;
import org.chocosolver.util.tools.ArrayUtils;

import java.util.Arrays;
import java.util.LinkedList;

public class PropCanonicity1 extends Propagator<IntVar> {
    private final int n;
    private final IStateInt alpha;
    private final IntVar[] pred;
    private final IntVar[] dir;
    private final IntVar[] x;

    private final IntVar[] y;

    private IStateInt[][] graph;

    private IStateInt[] pattern;

    private IStateInt[] current_code;

    private boolean is_catacondensed;

    public PropCanonicity1(IntVar[] Pred, IntVar[] Dir, IntVar[] X, IntVar[] Y, boolean is_catacondensed) {
        super(ArrayUtils.append(Pred, Dir, X, Y), PropagatorPriority.LINEAR, true);
        this.n = X.length;
        this.pred = Arrays.copyOfRange(vars, 0, n);
        this.dir = Arrays.copyOfRange(vars, n, 2*n);
        this.x = Arrays.copyOfRange(vars, 2*n, 3*n);
        this.y = Arrays.copyOfRange(vars, 3*n, 4*n);
        IEnvironment environment = model.getEnvironment();
        this.alpha = environment.makeInt(0);
        this.graph = new IStateInt[n][6];
        this.pattern = new IStateInt[n];
        this.current_code = new IStateInt[2*(n-1)];
        this.is_catacondensed = is_catacondensed;
        for(int i=0; i< n; i++){
            for(int j =0; j<6; j++){
                this.graph[i][j] = environment.makeInt(-1);
            }
            this.pattern[i] = environment.makeInt(-1);
        }

        for(int i=0; i<2*(n-1);i++) {
            this.current_code[i] = environment.makeInt(-1);
        }
    }

    @Override
    public void propagate(int evtmask) throws ContradictionException {
        boolean full_instantiated = true;
        for(int i=0; i<4*n; i++){
            if(!vars[i].isInstantiated()){
                full_instantiated = false;
                break;
            }
        }
        if(full_instantiated){
            int indice= 1;
            while (indice < n) {
                // incrementally construct the graph: first find the auxiliaire edges
                current_code[2*(indice-1)].set(pred[indice].getValue());
                current_code[2*(indice-1)+1].set(dir[indice].getValue());
                if(!updateGraph(indice)){
                    fails();
                }

                if(!checkCanonic(indice)){
                    fails();
                } else {
                    // update alpha
                    alpha.set(indice);
                    indice++;
                }
            }
        }
    }

    @Override
    public void propagate(int varIdx, int mask) throws ContradictionException{
        forcePropagate(PropagatorEventType.FULL_PROPAGATION);
    }

    @Override
    public ESat isEntailed() {
        if (isCompletelyInstantiated()) {
            for(int i=this.alpha.get(); i<n-1;i++){
                if(!updateGraph(i+1)){
                    return ESat.FALSE;
                }
                current_code[2*i].set(pred[i+1].getValue());
                current_code[2*i+1].set(dir[i+1].getValue());
                this.alpha.set(i+1);
            }
            for(int i=1; i<n; i++){
                if(!checkCanonic(i)){
                    return ESat.FALSE;
                }
            }
            return ESat.TRUE;
        }
        return ESat.UNDEFINED;
    }

    private boolean updateGraph(int i) {
        for (int j = 0; j < i; j++) {
            if (x[i].getValue() - x[j].getValue() == 2 && y[i].getValue() == y[j].getValue()) {
                this.graph[j][0].set(i);
                this.graph[i][3].set(j);
            } else if (x[i].getValue() - x[j].getValue() == 1 && y[i].getValue() - y[j].getValue() == 1) {
                this.graph[j][1].set(i);
                this.graph[i][4].set(j);
            } else if (x[i].getValue() - x[j].getValue() == -1 && y[i].getValue() - y[j].getValue() == 1) {
                this.graph[j][2].set(i);
                this.graph[i][5].set(j);
            } else if (x[i].getValue() - x[j].getValue() == -2 && y[i].getValue() == y[j].getValue()) {
                this.graph[j][3].set(i);
                this.graph[i][0].set(j);
            } else if (x[i].getValue() - x[j].getValue() == -1 && y[i].getValue() - y[j].getValue() == -1) {
                this.graph[j][4].set(i);
                this.graph[i][1].set(j);
            } else if (x[i].getValue() - x[j].getValue() == 1 && y[i].getValue() - y[j].getValue() == -1) {
                this.graph[j][5].set(i);
                this.graph[i][2].set(j);
            } else {
                continue;
            }
            updatePattern(j);
            updatePattern(i);
            if(this.is_catacondensed){
                if(this.pattern[i].get()<9 ){
                    return false;
                }
                if(this.pattern[j].get()<9){
                    return false;
                }
            }

        }
        // check holes
        return !containsHole(i);
    }

    private void updatePattern(int i){
        // 0: d_6 < 1: d_5 < 2: d_4(4) < 3: d_4(3+1) < 4: d_3(3) < 5: d_4(2+2) < 6: d_3(2+1a) = 7: d_3(2+1b) < 8: d_2(2) < 9: d_3(1+1+1) < 10: d_2(1+1a) < 11: d_2(1+1b) < 12: d_1
        // we treat 6,7 as the same case
        switch(this.pattern[i].get()){
            case 12:
                for(int j=0; j<6; j++){
                    if(this.graph[i][j].get()>=0 && this.graph[i][(j+1)%6].get()>=0){
                        this.pattern[i].set(8);
                        return;
                    }
                    if(this.graph[i][j].get()>=0 && this.graph[i][(j+3)%6].get()>=0){
                        this.pattern[i].set(11);
                        return;
                    }
                }

                this.pattern[i].set(10);
                break;
            case 11:
                this.pattern[i].set(6);
                break;

            case 10:
                for(int j=0; j<6; j++){
                    if(this.graph[i][j].get()>=0 && this.graph[i][(j+1)%6].get()>=0 && this.graph[i][(j+2)%6].get()>=0){
                        this.pattern[i].set(4);
                        return;
                    } else if (this.graph[i][j].get()>=0 && this.graph[i][(j+2)%6].get()>=0 && this.graph[i][(j+4)%6].get()>=0){
                        this.pattern[i].set(9);
                        return;
                    }
                }
                this.pattern[i].set(6);
                break;
            case 9:
                this.pattern[i].set(3);
                break;
            case 8:
                for(int j=0; j<6; j++){
                    if(this.graph[i][j].get()>=0 && this.graph[i][(j+1)%6].get()>=0 && this.graph[i][(j+2)%6].get()>=0){
                        this.pattern[i].set(4);
                        return;
                    }
                }

                this.pattern[i].set(6);
                break;
            case 6: // case 6 and case 7 are the same
                for(int j=0; j<6; j++){
                    if(this.graph[i][j].get()<0 && this.graph[i][(j+1)%6].get()<0){
                        this.pattern[i].set(2);
                        return;
                    } else if(this.graph[i][j].get()<0 && this.graph[i][(j+2)%6].get()<0){
                        this.pattern[i].set(3);
                        return;
                    }
                }

                this.pattern[i].set(5);
                break;
            case 5:
            case 3:
            case 2:
                this.pattern[i].set(1);
                break;
            case 4:
                for(int j=0; j<6; j++){
                    if(this.graph[i][j].get()<0 && this.graph[i][(j+1)%6].get()<0){
                        this.pattern[i].set(2);
                        return;
                    }
                }
                this.pattern[i].set(3);
                break;
            case 1:
                this.pattern[i].set(0);
                break;
            case -1:
                this.pattern[i].set(12);
                break;
            default:
                throw new AssertionError("pattern error");
        }
    }

    public boolean containsHole(int k){
        if(k<5){
            return false;
        }
        // we consider the vertex which has pattern in {1, 3, 5, 6,7, 9, 10}
        if(this.pattern[k].get()>10){
            return false;
        } else if(this.pattern[k].get()==1 || this.pattern[k].get()==3 || this.pattern[k].get()==5 || this.pattern[k].get()==9) {
            for(int d=0; d<6; d++){
                if(this.graph[k][d].get()==-1){
                    int d1 = (d+5)%6;
                    int v1 = this.graph[k][d1].get();
                    int d2 = (d+1)%6;
                    int v2 = this.graph[k][d2].get();
                    int v3 = this.graph[v1][d].get();
                    int v4 = this.graph[v2][d].get();

                    if(v3==-1 || v4==-1){
                        continue;
                    } else {
                        int v5 = this.graph[v3][d2].get();
                        int v6 = this.graph[v4][d1].get();
                        if(v5!=-1 && v6!=-1){
                            return true;
                        }
                    }
                }
            }
        } else if(this.pattern[k].get()==6 || this.pattern[k].get()==7 || this.pattern[k].get()==10) {
            for(int d=0; d<6; d++){
                if(this.graph[k][d].get()==-1 && this.graph[k][(d+5)%6].get()>=0 && this.graph[k][(d+1)%6].get()>=0){
                    int d1 = (d+5)%6;
                    int v1 = this.graph[k][d1].get();
                    int d2 = (d+1)%6;
                    int v2 = this.graph[k][d2].get();
                    int v3 = this.graph[v1][d].get();
                    int v4 = this.graph[v2][d].get();

                    if(v3==-1 || v4==-1){
                        break;
                    } else {
                        int v5 = this.graph[v3][d2].get();
                        int v6 = this.graph[v4][d1].get();
                        if(v5!=-1 && v6!=-1){
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    private boolean checkCanonic(int i){
        if(i<2){
            return true;
        } else {
            int k = 0;
            int min_pattern = this.pattern[0].get();
            while (k <= i) {
                if (this.pattern[k].get()<min_pattern) {
                    return false;
                } else if (this.pattern[k].get()==min_pattern) {

                    if (min_pattern == 0 || min_pattern == 9 || min_pattern == 11 || min_pattern == 12) {
                        for (int j = 0; j < 6; j++) {
                            if (this.graph[k][j].get() >= 0) {
                                int[] code1 = bfs(k, j, 0);
                                int[] code2 = bfs(k, j, 1);
                                if ((!compareSign(current_code, code1, i)) || (!compareSign(current_code, code2, i))) {
                                    return false;
                                }
                            }
                        }
                    } else if (min_pattern == 1 || min_pattern == 2 || min_pattern == 5 || min_pattern == 8) {
                        for (int j = 0; j < 6; j++) {
                            if (this.graph[k][j].get() >= 0 && this.graph[k][(j + 1) % 6].get() < 0) {
                                int[] code1 = bfs(k, j, 1);
                                if (!compareSign(current_code, code1, i)) {
                                    return false;
                                }
                            }
                            if (this.graph[k][j].get() < 0 && this.graph[k][(j + 1) % 6].get() >= 0) {
                                int[] code2 = bfs(k, j + 1, 0);
                                if (!compareSign(current_code, code2, i)) {
                                    return false;
                                }
                            }
                        }
                    } else if (min_pattern == 3 || min_pattern == 4) {
                        for (int j = 0; j < 6; j++) {
                            if (this.graph[k][j].get() >= 0 && this.graph[k][(j + 1) % 6].get() >= 0 && this.graph[k][(j + 5) % 6].get() < 0) {
                                int[] code1 = bfs(k, j, 0);
                                if ((!compareSign(current_code, code1, i))) {
                                    return false;
                                }
                            }

                            if (this.graph[k][j].get() >= 0 && this.graph[k][(j + 1) % 6].get() < 0 && this.graph[k][(j + 5) % 6].get() >= 0) {
                                int[] code2 = bfs(k, j, 1);
                                if ((!compareSign(current_code, code2, i))) {
                                    return false;
                                }
                            }
                        }
                    } else if (min_pattern == 6) {
                        for (int j = 0; j < 6; j++) {
                            if (this.graph[k][j].get() >= 0 && this.graph[k][(j + 1) % 6].get() >= 0 && this.graph[k][(j + 3) % 6].get() >= 0) {
                                int[] code1 = bfs(k, j, 0);
                                if (!compareSign(current_code, code1, i)) {
                                    return false;
                                }
                            }

                            if (this.graph[k][j].get() >= 0 && this.graph[k][(j + 1) % 6].get() >= 0 && this.graph[k][(j + 4) % 6].get() >= 0) {
                                int[] code2 = bfs(k, j + 1, 1);
                                if (!compareSign(current_code, code2, i)) {
                                    return false;
                                }
                            }
                        }
                    } else if (min_pattern == 10) {
                        for (int j = 0; j < 6; j++) {
                            if (this.graph[k][j].get() >= 0 && this.graph[k][(j + 2) % 6].get() >= 0) {
                                int[] code1 = bfs(k, j, 0);
                                if (!compareSign(current_code, code1, i)) {
                                    return false;
                                }
                            }
                            if (this.graph[k][j].get() >= 0 && this.graph[k][(j + 4) % 6].get() >= 0) {
                                int[] code2 = bfs(k, j, 1);
                                if (!compareSign(current_code, code2, i)) {
                                    return false;
                                }
                            }
                        }
                    }

                }
                k++;
            }
        }
        return true;
    }

    private int[] bfs(int s, int direction, int orientation){
        // s: source, direction : [0, 1, 2, 3, 4, 5], orientation: ccw=0, cw=1
        LinkedList<Integer> q = new LinkedList<>();
        boolean[] visited = new boolean[n];
        int[] visited_order = new int[n];
        int[] code = new int[2*n-2];
        int code_cnt = 0;
        int order = 0;
        q.add(s);
        visited_order[s] = order;
        order++;
        visited[s] = true;
        while (!q.isEmpty()) {
            int v = q.poll();
            for(int i=0; i<6;i++) {
                int index;
                if(orientation==0){
                    index = (i+direction)%6;
                } else {
                    index = (direction-i+6)%6;
                }

                int u = this.graph[v][index].get();
                if (u >= 0 && !visited[u]) {
                    visited[u] = true;
                    q.add(u);
                    visited_order[u]=order;
                    order++;
                    code[code_cnt++]=visited_order[v];
                    code[code_cnt++]=i;
                }
            }
        }
        return code;
    }

    private boolean compareSign(IStateInt[] c1, int[] c2, int num){
        /*the size of code is num*2,  return true if c1 <= c2 */
        int k= 0;
        while(k < num){
            if(c1[2*k].get() > c2[2*k]){
                return false;
            } else if (c1[2*k].get() < c2[2*k]) {
                return true;
            } else {
                if(c1[2*k+1].get() > c2[2*k+1]){
                    return false;
                } else if (c1[2*k+1].get() < c2[2*k+1]){
                    return true;
                }
            }
            k++;
        }
        return true;
    }

}
