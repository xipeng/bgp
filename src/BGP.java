import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.variables.IntVar;

import java.io.IOException;
import java.util.Objects;

public class BGP {
    public static void generate_pattern(int n, boolean is_cadacondensed,  String pattern1, String pattern2){
        Model model = new Model();
        Solver solver = model.getSolver();
        IntVar[] pred = new IntVar[n];
        IntVar[] dir = new IntVar[n];
        IntVar[] x = new IntVar[n];
        IntVar[] y = new IntVar[n];

        pred[0] = model.intVar("pred0", -1, -1);
        dir[0] = model.intVar("dir0", -1, -1);
        x[0] = model.intVar("x0", 0, 0);
        y[0] = model.intVar("y0", 0, 0);
        // Cell 1 is at coordinates (0,1) and its predecessor is 0
        pred[1] = model.intVar("pred1", 0, 0);
        dir[1] = model.intVar("dir1", 0, 0);
        x[1] = model.intVar("x1", 2, 2);
        y[1] = model.intVar("y1", 0, 0);

        Tuples forbidden_tuples = new Tuples(false);
        int pattern_length1;


        if(Objects.equals(pattern1, "deepbay")){
            pred[2] = model.intVar("pred2", 0, 0);
            dir[2] = model.intVar("dir2", 2, 2);
            x[2] = model.intVar("x2", -1, -1);
            y[2] = model.intVar("y2", 1, 1);

            pred[3] = model.intVar("pred3", 1, 1);
            dir[3] = model.intVar("dir3", 1, 1);
            x[3] = model.intVar("x3", 3, 3);
            y[3] = model.intVar("y3", 1, 1);

            forbidden_tuples.add(1, 1);
            forbidden_tuples.add(0, 2);
            forbidden_tuples.add(2, 2);
            pattern_length1 = 4;


        } else if(Objects.equals(pattern1, "a")){
            pred[2] = model.intVar("pred2", 0, 0);
            dir[2] = model.intVar("dir2", 2, 2);
            x[2] = model.intVar("x2", -1, 1);
            y[2] = model.intVar("y2", 1, 1);

            forbidden_tuples.add(-2, 0);
            forbidden_tuples.add(-1, -1);
            forbidden_tuples.add(1, -1);
            pattern_length1 = 3;


        } else if(Objects.equals(pattern1, "b")){
            pred[2] = model.intVar("pred2", 0, 0);
            dir[2] = model.intVar("dir2", 1, 1);
            x[2] = model.intVar("x2", 1, 1);
            y[2] = model.intVar("y2", 1, 1);

            forbidden_tuples.add(-1, 1);
            forbidden_tuples.add(-2, 0);
            forbidden_tuples.add(-1, -1);
            forbidden_tuples.add(1, -1);
            pattern_length1 = 3;

        }
        else if(Objects.equals(pattern1, "c")){
            forbidden_tuples.add(1, 1);
            forbidden_tuples.add(1, -1);
            forbidden_tuples.add(4, 0);
            forbidden_tuples.add(3, 1);
            forbidden_tuples.add(3, -1);
            pattern_length1 = 2;


        } else if(Objects.equals(pattern1, "d")){
            pred[2] = model.intVar("pred2", 0, 0);
            dir[2] = model.intVar("dir2", 2, 2);
            x[2] = model.intVar("x2", -1, -1);
            y[2] = model.intVar("y2", 1, 1);

            forbidden_tuples.add(1, 1);
            forbidden_tuples.add(3, 1);
            forbidden_tuples.add(0, 2);
            pattern_length1 = 3;


        } else if(Objects.equals(pattern1, "e")) {
            pred[2] = model.intVar("pred2", 0, 0);
            dir[2] = model.intVar("dir2", 2, 2);
            x[2] = model.intVar("x2", -1, -1);
            y[2] = model.intVar("y2", 1, 1);

            pred[3] = model.intVar("pred3", 1, 1);
            dir[3] = model.intVar("dir2", 1, 1);
            x[3] = model.intVar("x2", 3, 3);
            y[3] = model.intVar("y2", 1, 1);

            pred[4] = model.intVar("pred4", 2, 2);
            dir[4] = model.intVar("dir2", 1, 1);
            x[4] = model.intVar("x2", 0, 0);
            y[4] = model.intVar("y2", 2, 2);

            forbidden_tuples.add(1, 1);
            forbidden_tuples.add(4, 2);
            forbidden_tuples.add(1, 3);
            pattern_length1 = 5;

        } else if(Objects.equals(pattern1, "f")) {
            forbidden_tuples.add(1, 1);
            forbidden_tuples.add(-1, 1);
            forbidden_tuples.add(3, 1);
            pattern_length1 = 2;

        } else if(Objects.equals(pattern1, "g")) {
            pred[2] = model.intVar("pred2", 0, 0);
            dir[2] = model.intVar("dir2", 3, 3);
            x[2] = model.intVar("x2", -2, -2);
            y[2] = model.intVar("y2", 0, 0);
            forbidden_tuples.add(1, 1);
            forbidden_tuples.add(-1, 1);
            pattern_length1 = 3;

        } else if(Objects.equals(pattern1, "nsy")){
            pred[2] = model.intVar("pred2", 0, 0);
            dir[2] = model.intVar("dir2", 1, 1);
            x[2] = model.intVar("x2", 1, 1);
            y[2] = model.intVar("y2", 1, 1);

            pred[3] = model.intVar("pred3", 0, 0);
            dir[3] = model.intVar("dir3", 3, 3);
            x[3] = model.intVar("x3", -2, -2);
            y[3] = model.intVar("y3", 0, 0);
            forbidden_tuples.add(-1, -1);
            forbidden_tuples.add(1, -1);
            pattern_length1 = 4;

        }else if(Objects.equals(pattern1, " ")){
            pattern_length1=0;
        }else {
            System.out.println("pattern error");
            return;
        }

        int start_index = (pattern1.equals(" ")) ? 2 : pattern_length1;
        for (int i=start_index; i<n; i++){
            pred[i] = model.intVar("pred"+i, 0, i-1);
            dir[i] = model.intVar("dir"+i, 0, 5);
            x[i] = model.intVar("x"+i, -2*n, 2*n);
            y[i] = model.intVar("y"+i, -2*n, 2*n);
            model.table(new IntVar[] {x[i], y[i]}, forbidden_tuples).post();
        }

        start_index = (pattern1.equals(" ")) ? 1 : pattern_length1;
        for (int i=start_index; i<n-1; i++){
            model.arithm(pred[i],"<=",pred[i+1]).post();
            model.ifThen(model.arithm(pred[i],"=",pred[i+1]), model.arithm(dir[i],"<",dir[i+1]));
            if(is_cadacondensed){
                model.ifThen(model.arithm(pred[i],"=",pred[i+1]), model.arithm(dir[i+1], "-", dir[i] ,">",1));
            }
        }

        Tuples coordinates_tuples = new Tuples(true);
        for(int ix=-2*n; ix<=2*n-2; ix++){
            for(int iy=-n; iy<=n; iy++){
                coordinates_tuples.add(0, ix+2, ix, iy, iy);
            }
        }
        for(int ix=-2*n; ix<=2*n-1; ix++){
            for(int iy=-n; iy<=n-1; iy++){
                coordinates_tuples.add(1, ix+1, ix, iy+1, iy);
            }
        }
        for(int ix=-2*n+1; ix<=2*n; ix++){
            for(int iy=-n; iy<=n-1; iy++){
                coordinates_tuples.add(2, ix-1, ix, iy+1, iy);
            }
        }
        for(int ix=-2*n+1; ix<=2*n; ix++){
            for(int iy=-n; iy<=n; iy++){
                coordinates_tuples.add(3, ix-2, ix, iy, iy);
            }
        }
        for(int ix=-2*n+1; ix<=2*n; ix++){
            for(int iy=-n+1; iy<=n; iy++){
                coordinates_tuples.add(4, ix-1, ix, iy-1, iy);
            }
        }
        for(int ix=-2*n; ix<=2*n-1; ix++){
            for(int iy=-n+1; iy<=n; iy++){
                coordinates_tuples.add(5, ix+1, ix, iy-1, iy);
            }
        }

        for (int i=1; i<n; i++) {
            for (int j=0; j<i; j++) {
                // ensure that cells i and j have different coordinates
                model.or(model.arithm(x[i],"!=",x[j]),model.arithm(y[i],"!=",y[j])).post();
            }

            IntVar xpi = model.intVar(-2*n,2*n);
            IntVar ypi = model.intVar(-2*n,2*n);
            model.element(xpi, x, pred[i], 0).post(); // xpi = x[pred[i]]
            model.element(ypi, y, pred[i], 0).post(); // ypi = y[pred[i]]
            // post table constraint for the x,y coordinates
            model.table(new IntVar[] {dir[i], x[i], xpi, y[i], ypi}, coordinates_tuples).post();

        }


        IntVar[] nbSucc = new IntVar[n];
        for (int i=0; i<n; i++) {
            nbSucc[i] = model.intVar("nbSucc"+i, 0, n-1-i);
            model.among(nbSucc[i], pred, new int[] {i}).post();
        }

        if(pattern_length1>0){
            PropPrefixCanonicity canonicality = new PropPrefixCanonicity(pred, dir, x, y, pattern1, pattern2, is_cadacondensed);
            model.post(new Constraint("canonicality", canonicality));
            PropPrefixCanonicity1 canonicality1 = new PropPrefixCanonicity1(pred, dir, x, y, pattern1, pattern2, is_cadacondensed);
            model.post(new Constraint("canonicality1", canonicality1));
        } else {
            PropCanonicity canonicity = new PropCanonicity(pred, dir, x, y, nbSucc, is_cadacondensed);
            model.post(new Constraint("canonicality", canonicity));
            PropCanonicity1 canonicity1 = new PropCanonicity1(pred, dir, x, y, is_cadacondensed);
            model.post(new Constraint("canonicality1", canonicity1));
        }

        int nbSol = 0;
        while (solver.solve()) {

            nbSol++;

            System.out.print("Sol" + nbSol + " coord: ");
            for (int i = 0; i < n; i++)
                System.out.print("(" + x[i].getValue() + "," + y[i].getValue() + ") ");
            System.out.print("code: ");
            for (int i = 0; i < n - 1; i++) {
                System.out.print(pred[i+1].getValue()  + "/" + dir[i+1].getValue() + " ");

            }
            System.out.println();


        }
        //solver.printStatistics();
        System.out.println("nb sol = "+nbSol);
    }

    public static void main(String[] args) throws IOException {
        boolean is_cadacondensed = false;
        String[] patterns = new String[]{"c", "f", "a", "b", "d", "g", "deepbay", "e"};
        int[] pattern_size = new int[]{2, 2, 3, 3, 3, 3, 4, 5};


        // test two patterns
        for(int i=0; i<8; i++) {
            for (int j = 0; j <= i; j++) {
                for (int n = pattern_size[i] + pattern_size[j]; n < 11; n++) {
                    System.out.println("pattern1: " + patterns[i] + " pattern2: " + patterns[j] + " ,n=" + n);
                    generate_pattern(n, is_cadacondensed, patterns[i], patterns[j]);
                }
            }
        }


/*
        // test one pattern
        for(int i=0; i<8; i++) {
            for (int n = pattern_size[i]; n < 11; n++) {
                System.out.println("pattern1: " + patterns[i]  + ", n=" + n);
                generate_pattern(n, is_cadacondensed, patterns[i], " ", 3);
            }
        }


        // test
        for(int n=2; n<11; n++) {
            System.out.println("n= " + n);
            generate_pattern(n, is_cadacondensed, " ", " ", 3);
        }
*/
    }





}
